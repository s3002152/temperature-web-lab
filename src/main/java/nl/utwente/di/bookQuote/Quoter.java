package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String isbn) {
        double celsius = Double.parseDouble(isbn);
        return (celsius*9/5)+32.0;
    }
}
